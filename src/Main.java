/**
 * Copyright 2014 Ivan Nevolin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import vtk.*;

public class Main {

    static {
        vtkNativeLibrary.LoadAllNativeLibraries();
    }

    private final static double [] TABLETOP_CENTER = {0, 0, 2.0};
    private final static double [] TABLETOP_DIMENSIONS = {4.0, 2.5, 0.1};
    private final static double [] TABLETOP_TEXTURE_TRANSLATE = {3.0, 3.0, 3.0};
    private final static double [] TABLETOP_TEXTURE_SCALE = {0.1, 0.1, 0.1};

    private final static double [][] LEGS_POINTS = {
            {1.8, 0.9, 2},
            {1.9, 1.25, 0},
            {-1.8, 0.9, 2},
            {-1.9, 1.25, 0},
            {-1.8, -0.9, 2},
            {-1.9, -1.25, 0},
            {1.8, -0.9, 2},
            {1.9, -1.25, 0}
    };
    private final static double LEG_RADIUS = 0.1;
    private final static int LEG_QUALITY = 8;

    private final static double [] TIGER_FIGHTER_TRANSLATE = {0, 0.35, 2.5};
    private final static double [] TIGER_FIGHTER_SCALE = {0.01, 0.01, 0.01};
    private final static double [] TIGER_FIGHTER_ANGLES = {90, 60, 0};

    private final static double [] TYRO_TRANSLATE = {1.5, 0.5, 2.05};
    private final static double [] TYRO_SCALE = {0.01, 0.01, 0.01};
    private final static double [] TYRO_ANGLES = {90, 190, 0};

    private final static double [] TWO_BANANAS_TRANSLATE = {-1.0, 0, 2.3};
    private final static double [] TWO_BANANAS_SCALE = {0.005, 0.005, 0.005};
    private final static double [] TWO_BANANAS_ANGLES = {0, 90, 0};

    private final static double [] LIGHT_POSITION = {0, 0, 5};
    private final static double [] LIGHT_FOCAL_POINT = {0, 0, 0};
    private final static double LIGHT_CONE_ANGLE = 40;
    private final static double [] LIGHT_DIFFUSE_COLOR = {1, 0.5, 0.5};

    private final static double [] LIGHT2_POSITION = {0, -1, 0};
    private final static double [] LIGHT2_FOCAL_POINT = {0, 0, 2};
    private final static double LIGHT2_CONE_ANGLE = 20;
    private final static double [] LIGHT2_DIFFUSE_COLOR = {1, 1, 1};
    
    private final static double [] BACKGROUND_COLOR = {0.1, 0.3, 0.4};

    private final static double [] CAMERA_POSITION = {-2.5, -2.0, 3.0};
    private final static double [] CAMERA_FOCAL_POINT = {0.2, 0.5, 2.0};
    private final static double CAMERA_ROLL_ANGLE = 90;

    private final static int [][] WINDOW_BOUNDS = {
            {400, 100},
            {600, 600}
    };

    /**
     * Adopted from http://www.paraview.org/Wiki/VTK/Examples/Cxx/Visualization/Shadows
     */
    private static vtkCameraPass shadowCameraPass() {
        vtkTranslucentPass translucent = new vtkTranslucentPass();

        vtkDepthPeelingPass peeling = new vtkDepthPeelingPass();
        peeling.SetMaximumNumberOfPeels(200);
        peeling.SetOcclusionRatio(0.1);
        peeling.SetTranslucentPass(translucent);

        vtkLightsPass lights = new vtkLightsPass();

        vtkOpaquePass opaque = new vtkOpaquePass();

        vtkRenderPassCollection passes2 = new vtkRenderPassCollection();
        passes2.AddItem(lights);
        passes2.AddItem(opaque);

        vtkSequencePass opaqueSequence = new vtkSequencePass();
        opaqueSequence.SetPasses(passes2);

        vtkCameraPass opaqueCameraPass = new vtkCameraPass();
        opaqueCameraPass.SetDelegatePass(opaqueSequence);

        vtkShadowMapBakerPass shadowsBaker = new vtkShadowMapBakerPass();
        shadowsBaker.SetOpaquePass(opaqueCameraPass);
        shadowsBaker.SetResolution(1024);
        // To cancel self-shadowing.
        shadowsBaker.SetPolygonOffsetFactor(3.1f);
        shadowsBaker.SetPolygonOffsetUnits(10.0f);

        vtkShadowMapPass shadows = new vtkShadowMapPass();
        shadows.SetShadowMapBakerPass(shadowsBaker);
        shadows.SetOpaquePass(opaqueSequence);

        vtkVolumetricPass volume = new vtkVolumetricPass();

        vtkOverlayPass overlay = new vtkOverlayPass();

        vtkRenderPassCollection passes = new vtkRenderPassCollection();
        passes.AddItem(shadowsBaker);
        passes.AddItem(shadows);
        passes.AddItem(lights);
        passes.AddItem(peeling);
        passes.AddItem(volume);
        passes.AddItem(overlay);

        vtkSequencePass seq = new vtkSequencePass();
        seq.SetPasses(passes);

        vtkCameraPass cameraP = new vtkCameraPass();
        cameraP.SetDelegatePass(seq);

        return cameraP;
    }

    private static void addTable(vtkRenderer renderer) {
        //tabletop
        vtkCubeSource tabletop = new vtkCubeSource();
        tabletop.SetCenter(TABLETOP_CENTER);
        tabletop.SetXLength(TABLETOP_DIMENSIONS[0]);
        tabletop.SetYLength(TABLETOP_DIMENSIONS[1]);
        tabletop.SetZLength(TABLETOP_DIMENSIONS[2]);

        vtkPolyDataMapper tabletopMapper = new vtkPolyDataMapper();
        tabletopMapper.SetInputConnection(tabletop.GetOutputPort());

        vtkJPEGReader tabletopTextureReader = new vtkJPEGReader();
        tabletopTextureReader.SetFileName("resource/TableTextures/tabletop.jpg");
        tabletopTextureReader.Update();

        vtkTransform tabletopTextureTransform = new vtkTransform();
        tabletopTextureTransform.Scale(TABLETOP_TEXTURE_SCALE);
        tabletopTextureTransform.Translate(TABLETOP_TEXTURE_TRANSLATE);

        vtkTexture tabletopTexture = new vtkTexture();
        tabletopTexture.SetInputConnection(tabletopTextureReader.GetOutputPort());
        tabletopTexture.RepeatOff();
        tabletopTexture.SetTransform(tabletopTextureTransform);

        vtkActor tabletopActor = new vtkActor();
        tabletopActor.SetMapper(tabletopMapper);
        tabletopActor.SetTexture(tabletopTexture);
        //end tabletop

        //legs
        vtkPoints points = new vtkPoints();
        points.Allocate(8, 8);
        for (double[] legPoint : LEGS_POINTS) points.InsertNextPoint(legPoint);

        vtkCellArray cellArray = new vtkCellArray();
        cellArray.Allocate(8, 8);
        for (int i = 0; i < 8; i += 2) {
            cellArray.InsertNextCell(2);
            cellArray.InsertCellPoint(i);
            cellArray.InsertCellPoint(i + 1);
        }

        vtkPolyData polyData = new vtkPolyData();
        polyData.Allocate(8, 8);
        polyData.SetLines(cellArray);
        polyData.SetPoints(points);

        vtkTubeFilter legs = new vtkTubeFilter();
        legs.AddInput(polyData);
        legs.SetRadius(LEG_RADIUS);
        legs.SetNumberOfSides(LEG_QUALITY);
        legs.CappingOn();
        legs.Update();

        vtkPolyDataMapper legsMapper = new vtkPolyDataMapper();
        legsMapper.SetInput(legs.GetOutput());

        vtkJPEGReader legsTextureReader = new vtkJPEGReader();
        legsTextureReader.SetFileName("resource/TableTextures/legs.jpg");
        legsTextureReader.Update();

        vtkTexture legsTexture = new vtkTexture();
        legsTexture.SetInputConnection(legsTextureReader.GetOutputPort());

        vtkActor legsActor = new vtkActor();
        legsActor.SetMapper(legsMapper);
        legsActor.SetTexture(legsTexture);
        //end legs

        vtkAssembly table = new vtkAssembly();
        table.AddPart(tabletopActor);
        table.AddPart(legsActor);

        renderer.AddActor(table);
    }

    private static void addObject(vtkRenderer renderer,
                                    String name,
                                    double [] translate,
                                    double [] scale,
                                    double [] angles) {
        vtkOBJReader objReader = new vtkOBJReader();
        objReader.SetFileName("resource/" + name + "/" + name + ".obj");
        objReader.Update();

        vtkPolyDataMapper objMapper = new vtkPolyDataMapper();
        objMapper.SetInputConnection(objReader.GetOutputPort());

        vtkJPEGReader objTextureReader = new vtkJPEGReader();
        objTextureReader.SetFileName("resource/" + name + "/" + name + "_0.jpg");
        objTextureReader.Update();

        vtkTexture objTexture = new vtkTexture();
        objTexture.SetInputConnection(objTextureReader.GetOutputPort());

        vtkTransform objTransform = new vtkTransform();
        objTransform.Translate(translate);
        objTransform.Scale(scale);
        if (angles[0] > 0) objTransform.RotateX(angles[0]);
        if (angles[1] > 0) objTransform.RotateY(angles[1]);
        if (angles[2] > 0) objTransform.RotateZ(angles[2]);

        vtkActor objActor = new vtkActor();
        objActor.SetMapper(objMapper);
        objActor.SetTexture(objTexture);
        objActor.SetUserTransform(objTransform);

        renderer.AddActor(objActor);
    }

    private static void addPositionalLight(vtkRenderer renderer,
                                           double[] lightPosition,
                                           double[] lightFocalPoint,
                                           double lightConeAngle,
                                           double[] lightColor) {
        vtkLight light = new vtkLight();
        light.SetLightTypeToSceneLight();
        light.SetPosition(lightPosition);
        light.SetPositional(1);
        light.SetConeAngle(lightConeAngle);
        light.SetFocalPoint(lightFocalPoint);
        light.SetDiffuseColor(lightColor);

        vtkLightActor lightActor = new vtkLightActor();
        lightActor.SetLight(light);

        renderer.AddViewProp(lightActor);
        renderer.AddLight(light);
    }

    private static void positionCamera(vtkRenderer renderer) {
        renderer.GetActiveCamera().SetPosition(CAMERA_POSITION);
        renderer.GetActiveCamera().SetFocalPoint(CAMERA_FOCAL_POINT);
        renderer.GetActiveCamera().Roll(CAMERA_ROLL_ANGLE);
        renderer.ResetCamera();
    }

    public static void main(String[] args) throws Exception {
        vtkRenderer renderer = new vtkOpenGLRenderer();

        addTable(renderer);
        addObject(renderer, "TigerFighter", TIGER_FIGHTER_TRANSLATE, TIGER_FIGHTER_SCALE, TIGER_FIGHTER_ANGLES);
        addObject(renderer, "TwoBananas", TWO_BANANAS_TRANSLATE, TWO_BANANAS_SCALE, TWO_BANANAS_ANGLES);
        addObject(renderer, "Tyro", TYRO_TRANSLATE, TYRO_SCALE, TYRO_ANGLES);

        addPositionalLight(renderer, LIGHT_POSITION, LIGHT_FOCAL_POINT, LIGHT_CONE_ANGLE, LIGHT_DIFFUSE_COLOR);
        addPositionalLight(renderer, LIGHT2_POSITION, LIGHT2_FOCAL_POINT, LIGHT2_CONE_ANGLE, LIGHT2_DIFFUSE_COLOR);

        renderer.SetBackground(BACKGROUND_COLOR);
        renderer.SetGradientBackground(true);

        positionCamera(renderer);

        vtkRenderWindow window = new vtkRenderWindow();
        window.AddRenderer(renderer);
        window.SetPosition(WINDOW_BOUNDS[0]);
        window.SetSize(WINDOW_BOUNDS[1]);
        window.SetMultiSamples(0);
        window.SetAlphaBitPlanes(1);

        // "FBOs are not supported by the context. Cannot use shadow mapping." error
        if (new vtkFrameBufferObject().IsSupported(window))
            renderer.SetPass(shadowCameraPass());

        vtkInteractorStyle style = new vtkInteractorStyleSwitch();

        vtkRenderWindowInteractor interactor = new vtkRenderWindowInteractor();
        interactor.SetRenderWindow(window);
        interactor.SetInteractorStyle(style);

        interactor.Initialize();

        window.Render();

        interactor.Start();
    }
}
